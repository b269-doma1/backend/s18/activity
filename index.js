
	/*1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

function sum(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of 5 and 15:");
	console.log(sum);
}

sum(5, 15);

function difference(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed diffrence of 5 and 20:");
	console.log(difference);
}

difference(20, 5);


/*Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.
	*/

function product(num1, num2){
	let product = num1 * num2;
	return product
}

let myProduct = product(50, 10);
console.log("The product of 50 and 10:");
console.log(myProduct);




/*Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.
*/

function quotient(num1, num2){
	let quotient = num1 / num2;
	return quotient
}

let myQuotient = quotient(50, 10);
console.log("The quotient of 50 and 10:");
console.log(myQuotient);



/*Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/



